// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.
// Необов'язкове завдання підвищеної складності
// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
// Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.

const GET_USERS_URL = "https://ajax.test-danit.com/api/json/users/";
const GET_POSTS_URL = "https://ajax.test-danit.com/api/json/posts/";

class CardApi {
  constructor() {}
  async getUsers() {
    return fetch(GET_USERS_URL, { method: "GET" })
      .then((r) => r.json())
      .then((data) => data)
      .catch((error) => console.log("Error", error));
  }

  async getPosts(users) {
    console.log(users);
    return fetch(GET_POSTS_URL, { method: "GET" })
      .then((r) => r.json())
      .then((data) => {
        console.log("Posts:", data);
        let list = document.body.querySelector("#output");
        users.forEach((user) => {
          let filterPost = data.filter((post) => {
            return post.userId === user.id;
          });

          filterPost.forEach((post) => {
            list.innerHTML += `
            <div id=${post.id}>
            <ul>
                <li>Name:${user.name}</li>
                <li>Email:${user.email}</li>
            </ul>
            <div>
                <h3>${post.title}</h3>
                <p>${post.body}</p>
            </div> ${button(post.id)}
            </div>`;
          });
        });
      })
      .catch((error) => console.log("Error", error));
  }
}

const card = new CardApi();

card
  .getUsers()
  .then((users) => {
    console.log("user", users);
    card.getPosts(users);
  })
  .catch((err) => {
    console.log("error", err);
  });

function button(id) {
  return `<button data-id=${id}>Delete</button>`;
}

document.addEventListener("click", (e) => {
  if (e.target.dataset.id) {
    const decision = confirm("Are sure to delete ?");
    if (decision) {
      const id = e.target.getAttribute("data-id");
      const div = document.getElementById(id);
      div.remove();
    }
  }
});

function remove() {
  fetch(GET_POSTS_URL / `${postId}`, {
    method: "DELETE",
    headers: {
      "Content-type": "application/json",
    },
  })
    .then((r) => {
      if (r.ok) {
        confirm("Are you sure to delete ?");
      } else {
        return r;
      }
    })
    .catch((error) => console.log("Error", error));
}

// async function getUsers() {
//   await fetch("https://ajax.test-danit.com/api/json/users/")
//     .then((r) => r.json())
//     .then((data) => {
//       console.log("Users:", data);
//       let list = document.body.querySelector("#output");
//       data.forEach(function (user) {
//         list.innerHTML += `
//       <ul>
//           <li>Name:${user.name}</li>
//           <li>Email:${user.email}</li>
//       </ul>`;
//       });
//     });
// }

// async function getPosts() {
//   await fetch("https://ajax.test-danit.com/api/json/posts/")
//     .then((r) => r.json())
//     .then((data) => {
//       console.log("Posts:", data);
//       let list = document.body.querySelector("#output");
//       data.forEach(function (post) {
//         list.innerHTML += `
//       <div>
//           <h3>${post.title}</h3>
//           <p>${post.body}</p>
//       </div>`;
//       });
//     });
// }
